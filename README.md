FoMaR Assignment 2 - Jack Magee
Readme for assignment 2, which takes in triangle soup file, converts it to vertex-face form, and then converts to a directed edge form.

To compile:
In terminal
Open directory where files are located.
Run 'make all'.

To generate face file from a .tri file:
Type ./face2faceindex FILENAME.tri
This will generate the .face file in the directory where .cpp files are stored

To generate .diredge file from a .face file:
Type .faceindex2directededge FILENAME.face
This will generate the .diredge file in the directory where .cpp files are stored.

If there are any issues with the file, this will be brought up when generating the .diredge file.
In the program it will check for any bad edges and if there are any pinch points. If the edges are bad then it will output which vertex they are bad at. If there are any pinch points then it will also say in the terminal. If either of these errors occur then the program will stop. Show the error in terminal and no .diredge file will be created.

Examples:

./face2faceindex models/legoman.tri 
./faceindex2directededge legoman.face
Error with mesh at vertex:147

./face2faceindex models/tetrahedron.tri
./faceindex2directededge tetrahedron.face
Any Pinches?: 0
Genus: 0

The genus will give the correct answer if the inputted mesh is connected, it does not work for disconnected meshes.
The complexity for each part is written within the comments with total complexity per file written at the end.