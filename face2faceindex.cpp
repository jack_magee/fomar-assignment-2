#include <iostream>
#include <fstream>
#include <vector>
#include <libgen.h>
#include <string.h>

using namespace std;

bool isElement(vector<float> unique_vertices, vector<float> point, int no_triangles);
vector<int> getIndex(vector<float> unique_vertices, vector<float> all_points);


int main(int argc, char** argv)
//The main call for the program, this takes an input as the .tri file and will output the .face file
{
    //This part takes in the input file
    ifstream src(argv[1]);
    int no_triangles;
    src >> no_triangles; // from the first line, gets number of triangles
    std::vector<float> triangles; //vector to store all triangle coords

    //used to get the filename from the input, used for saving file and in header
    char *bname, *path = argv[1];
    bname = basename(path); //gets base filename with extension
    string file_name(bname);
    file_name = file_name.substr(0,file_name.size()-4); //remove extension

    float val; //the current value of the file
    //goes through all elements in the soup file. O(n)
    if (!src) { 
        std::cerr << "Cannot open triangles file." << std::endl; //if the file cant be opened or is empty, disp error.
        return 0; 
    }
    while (src >> val){
        triangles.push_back(val); //for all the values in the file, push into a vector, doesnt care if unique or not
    }

    std::vector<float> unique_vertices; //init vector to store all the unique vertices

    //Used to find all the unique vertices in the vector of all vertices
    //Loops through all the triangles, O(9n^2) = O(n^2)
    for(int i = 0; i<no_triangles; i++){
        for(int j = 0; j < 3; j++){
            std::vector<float> point_to_check; //creates a vector of the 3 coords of the vertex for each vertex
            for(int k = 0; k <3; k++){
                point_to_check.push_back(triangles[i*3*3 + j*3 + k]);
            }
            //If the unique_vertex array is empty, need to add the first point
            if(unique_vertices.size() <3){
                unique_vertices.push_back(point_to_check[0]);
                unique_vertices.push_back(point_to_check[1]);
                unique_vertices.push_back(point_to_check[2]);
                continue;
            }
            //if it isn't empty then check to see if the point to check is in the vector
            if(isElement(unique_vertices, point_to_check, unique_vertices.size()/3) == false){ // if not in the vector, add it to the end
                unique_vertices.push_back(point_to_check[0]);
                unique_vertices.push_back(point_to_check[1]);
                unique_vertices.push_back(point_to_check[2]);
            }
        }
    }
    //Goes through all points in 2 loops. O(n^2)
    vector<int> face_form = getIndex(unique_vertices, triangles); //this turns all the triangles into face form.

    //Section of code used for generating the .face file
    ofstream myfile;
    string name = file_name + ".face";
    myfile.open (name.c_str());
    myfile << "# University of Leeds 2019-2020\n";
    myfile << "# COMP 5812M Assignment 2\n";
    myfile << "# Jack Magee\n";
    myfile << "# 201387459\n";
    myfile << "#\n";
    myfile << "# Object Name: "; myfile << file_name; myfile << "\n";
    myfile << "# Vertices="; myfile << unique_vertices.size()/3;
    myfile << " Faces="; myfile << face_form.size()/3; myfile << "\n";
    myfile << "#\n";
    for(int i = 0; i < unique_vertices.size()/3; i++){
        myfile << "Vertex "; myfile << i; myfile << " ";
        myfile << unique_vertices[i*3+0]; myfile << " ";
        myfile << unique_vertices[i*3+1]; myfile << " ";
        myfile << unique_vertices[i*3+2]; myfile << "\n";
    }
    for(int i = 0; i < face_form.size()/3; i++){
        myfile << "Face "; myfile << i; myfile << " ";
        myfile << face_form[i*3+0]; myfile << " ";
        myfile << face_form[i*3+1]; myfile << " ";
        myfile << face_form[i*3+2]; myfile << "\n";
    }
    myfile.close();

    return 0;
}

//A function to take in a vector of all unique points and a single point, and check whether the point is in the vector
//each time checks through all points so is O(n)
bool isElement(vector<float> unique_vertices, vector<float> point, int no_points)
{
    for(int i = 0; i < no_points; i++){//goes through all the points in blocks of three and compares to point to check to see if they match
        if((unique_vertices[i*3]==point[0])&&(unique_vertices[i*3 +1]==point[1])&&(unique_vertices[i*3 +2]==point[2])){
            return true;
        }
    }
    return false;
}

//A function to get the index for all triangle points from the unique points and store it in face form
vector<int> getIndex(vector<float> unique_vertices, vector<float> all_points)
{
    vector<int> index_form;
    for(int i = 0; i < all_points.size()/3; i++){ //goes through all the points and finds which unique point it is and allocated that the respective index
        vector<float> current_point;
        current_point.push_back(all_points[i*3 + 0]);
        current_point.push_back(all_points[i*3 + 1]);
        current_point.push_back(all_points[i*3 + 2]);
        for(int j = 0; j < unique_vertices.size()/3; j++){
            if((unique_vertices[j*3]==current_point[0])&&(unique_vertices[j*3 +1]==current_point[1])&&(unique_vertices[j*3 +2]==current_point[2])){
                index_form.push_back(j);
                continue;
            }
        }
    }
    return index_form;
}
//The final complexity of the program is dominated by O(n^2) where n is the total vertices.