#include <iostream>
#include <fstream>
#include <vector>
#include <libgen.h>
#include <string.h>

using namespace std;

//define all functions to be used
int find_other_half(vector<int> pair_to_find, vector<int> face_form);
bool is_pinch_point(int vertex, vector<int> directed_edges, vector<int> other_half, vector<int> face_form);
int next_vertex(int previous);
int get_genus(int v, int f, int e);

int main(int argc, char** argv)
{
    char type[7]; //used to store type from .face file
    string Vertex = "Vertex"; //both types
    string Face = "Face";

    float x,y,z; //coords fo unique vertex
    int f1,f2,f3, v_no, f_no; //face form and number of vectors and faces
    vector<float> unique_vertices;
    vector<int> face_form;
    vector<int> directed_edges; //to store directed edges
    vector<int> other_half; //to store other half of each edge
    bool any_pinches = false;
    char buffer[100]; //for each line of .face

    //Goes through full file and adds to respective vector, O(n)
    FILE* f = fopen(argv[1], "r");
    while (true)
    {
        if(fgets(buffer, 100, f) == NULL) break; //if empty
        if(buffer[0] == '#'){ //if # then header
            continue;
        }
        else
        {
            sscanf(buffer, "%s", type); // check type
            if(type == Vertex){ //store unique vetices
                sscanf(buffer, "%s %d %f %f %f", type, &v_no, &x, &y, &z);
                unique_vertices.push_back(x);
                unique_vertices.push_back(y);
                unique_vertices.push_back(z);
            }
            if(type == Face){ //store face form
                sscanf(buffer, "%s %d %d %d %d", type, &f_no, &f1, &f2, &f3);
                face_form.push_back(f1);
                face_form.push_back(f2);
                face_form.push_back(f3);
            }
        }
    }
    fclose(f);

    //to find the first directed edge for each vertex
    //go through all unique vertex, and check to find in face_form O(n^2)
    for(int i = 0; i < unique_vertices.size()/3; i++){ // go through each vertex
        bool flag = false;
        int j = 0;
        while(flag==false){
            if(i == face_form[j]){ // if the vertex == to point in face_form then thats the corresponding directed edge
                directed_edges.push_back(j);
                flag = true;
            }
            else{
                j++; // ifnot then check next value in face form
            }
        }
    }

    //for finding otherhalf to each edge
    //O(n^2) as goes through each edge and calls find other half
    for(int i = 0; i < face_form.size()/3; i++){ //go through each edge in face form
        for(int j = 0; j < 3; j++){
            vector<int> pair_to_check; //generate each edge
            pair_to_check.push_back(face_form[i*3+j]);
            pair_to_check.push_back(face_form[i*3 + (j+1)%3]);
            
            int val = find_other_half(pair_to_check, face_form); //find the other half edge in the rest of the list
            if(val == -1){ //if it cant find then theres an error at vertex...
                cout << pair_to_check[0] << " " << pair_to_check[1] << "\n";
                cout<<"Error with mesh at vertex:" << i*3 + j << "\n";
                return 0;
            }
            else{
                other_half.push_back(val); //if found then add to other half edge list.
            }
        }
    }
    
    //used to check if each vertex has a pinch point, if so it shall break
    //each call is O(n) and goes through all n, so O(n^2)
    for(int i = 0; i < unique_vertices.size()/3; i++){
        bool is_pinch = is_pinch_point(i,directed_edges, other_half, face_form);
        if(is_pinch == true){
            cout << i << "\n";
            any_pinches = true;
            break;
        }
    }
    
    cout << "Any Pinches?: " << any_pinches << "\n";
    if(any_pinches == true){
        return 0;
    }

    int genus = get_genus(unique_vertices.size()/3, face_form.size()/3, other_half.size()/2); //get genus of mesh
    cout << "Genus: " << genus << "\n";

    //section used to output the .diredge file with all the required information
    char *bname, *path = argv[1];
    bname = basename(path);
    string file_name(bname);
    file_name = file_name.substr(0,file_name.size()-5);

    ofstream myfile;
    string name = file_name + ".diredge";
    myfile.open (name.c_str());
    myfile << "# University of Leeds 2019-2020\n";
    myfile << "# COMP 5812M Assignment 2\n";
    myfile << "# Jack Magee\n";
    myfile << "# 201387459\n";
    myfile << "#\n";
    myfile << "# Object Name: "; myfile << file_name; myfile << "\n";
    myfile << "# Vertices="; myfile << unique_vertices.size()/3;
    myfile << " Faces="; myfile << face_form.size()/3; myfile << "\n";
    myfile << "#\n";
    for(int i = 0; i < unique_vertices.size()/3; i++){
        myfile << "Vertex "; myfile << i; myfile << " ";
        myfile << unique_vertices[i*3+0]; myfile << " ";
        myfile << unique_vertices[i*3+1]; myfile << " ";
        myfile << unique_vertices[i*3+2]; myfile << "\n";
    }
    for(int i = 0; i < directed_edges.size(); i++){
        myfile << "FirstDirectedEdge "; myfile << i; myfile << " ";
        myfile << directed_edges[i]; myfile << "\n";
    }
    for(int i = 0; i < face_form.size()/3; i++){
        myfile << "Face "; myfile << i; myfile << " ";
        myfile << face_form[i*3+0]; myfile << " ";
        myfile << face_form[i*3+1]; myfile << " ";
        myfile << face_form[i*3+2]; myfile << "\n";
    }
    for(int i = 0; i < other_half.size(); i++){
        myfile << "OtherHalf "; myfile << i; myfile << " ";
        myfile << other_half[i]; myfile << "\n";
    }
    myfile.close();
    return 0;
}

//Function to find the other half edge ofa given edge.
//goes through each edge O(n)
int find_other_half(vector<int> pair_to_find, vector<int> face_form)
{
    for(int i = 0; i < face_form.size()/3; i++){ //find set of edges to look at
        int v1 = face_form[i*3 + 0];
        int v2 = face_form[i*3 + 1];
        int v3 = face_form[i*3 + 2];
        int p1 = pair_to_find[1]; //flip point so only need to find match
        int p2 = pair_to_find[0];
        
        if((v1==p1)&&(v2==p2)){ //check each edge in set, if found retrun edge, else move to next set
            return i*3 + 0;
        }
        if((v2==p1)&&(v3==p2)){
            return i*3 + 1;
        }
        if((v3==p1)&&(v1==p2)){
            return i*3 + 2;
        }
    }
    return -1;
}

//function to see if there is a pinch point at a vertex
bool is_pinch_point(int vertex, vector<int> directed_edges, vector<int> other_half, vector<int> face_form){
    int degree = 0; //first find the degree at the vertex
    for(int i = 0; i < face_form.size(); i++){
        if(vertex == face_form[i]){
            degree++;   
        }
    }
    int counter = 1; //now find the number of edges in a loop around it
    int first_edge = directed_edges[vertex]; //go along directed edges and the half edge, all way around untill back at start
    int n_v = next_vertex(other_half[first_edge]);
    while(first_edge != n_v){
        counter++;
        n_v = next_vertex(other_half[n_v]);
    }
    if(degree == counter){ // if they are same, then no pinch point, else there is
        return false;
    }
    else{
        return true;
    }
}
 //function to find next vertex in a triangle
int next_vertex(int previous){
    int p = previous;
    int a = p / 3;
    int b = (p + 1)%3;
    return (a*3+b);
}

//function to get the genus of the mesh
int get_genus(int v, int f, int e)
{
    return (v-e+f-2)/-2; //using euler formula
}
//This program is dominated again by O(n^2).